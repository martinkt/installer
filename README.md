# Installer

Shellový skript, který má usnadnit instalaci aplikací na vzdálené servery.

Instalace je rozdělena na 2 části, nejprve se vytvoří distribuční adresář (distr).
Předpokládá, že to zajistí externí script makedistr. Ten je závislý na typu úlohy, může vypadat např. takto:

```
#!/bin/bash


QTDIR=/home/martin/devel/tools/Qt5.5.0/5.5/gcc_64/

# kompilace C++ casti
[ -d release ] || mkdir release
cd release
${QTDIR}/bin/qmake ../ruian.pro -r CONFIG+=release CONFIG-=debug && make
if [ $? -ne 0 ]
then
  echo "Compilation error"
  exit 1
fi
cd ..

# vytvoreni adresare distr
[ -d distr ] || mkdir distr
[ -d distr/lib ] || mkdir distr/lib

# nakopirovani zkompilovanych souboru do adresare distr
cp -a release/lib/libruian* distr/lib
cp -a release/zpracujruian/zpracujruian distr/

# zbuildovani Angular casti
cd crossbarconsole
bower install || exit 1
grunt build || exit 1
cd ..

# nakopirovani Angular cati do adresare distr
[ -d distr/console ] || mkdir distr/console
rsync -a  crossbarconsole/dist/ distr/console
````

Po vytvoření distribučního adresáře se provede jeho nakopírování na server (pomoci rsync) a provedení instalace
na vzdáleném serveru. Protože instalace na každém serveru může být jiná, je tato část definovaná
v samostatném adresáři pro každý cíl, na který se instalace provádí.

Např. vytvořím adresář *devel.install*, kde bude definována instalace na server devel. První část
názvu adresáře se potom zadává jako parametr skriptu install, takže v tomto případě budu pouštět
instalaci příkazem `install devel`.
Instalační adresář musí obsahovat soubor

`config.sh`, který obsahuje definici proměnných:

```
TARGET_SERVER=devel    # jmeno serveru na ktery se instaluje  (muze obsahovat jmeno uzivatele user@server)
TARGET_DIR=ruian       # adresar do ktereho se instaluje (muze byt absolutni cesta nebo relativni - k domovskemu adresari uzivatele)
REMOTE_RSYNC="sudo rsync" #prikaz rsync, ktery se spusti na vzdalenem serveru

COPY_FILES=copyfiles.txt  # volitelne - jmeno souboru, ktery obsahuje seznam instalacne zavislych souboru, ktere se maji uploadovat na server

REMOTE_INSTALL=install.sh # volitelne - jmeno souboru, ktery se po uploadu spusti na vzdalenem serveru a provede instalaci
```

`distr.sh` (volitelně) - obsahuje definici proměnných pro skript makedistr

Dále může obsahovat soubory, které se mají uploadovat na server, např. soubory pro konfiguraci crontabu, init scripty, apod.
Instalační script může obsahovat aktualizaci databázových struktur, restart služby apod.
